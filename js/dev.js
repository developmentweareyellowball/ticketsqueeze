jQuery(document).ready(function(){
    
    jQuery('.scroll-down').on('click', function(event) {
          event.preventDefault();
          jQuery('html, body').animate({
            scrollTop: jQuery(this).parents('section').next('section').offset().top - 40
          }, 800);
        return false;
      });
      jQuery('.featured-events-slider').slick({
          dots: false,
          infinite: false,
          speed: 800,
          slidesToShow: 2,
          slidesToScroll: 1, 
          responsive: [
              {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
                arrows:false,  
              }
            },
          ]
        });
    jQuery('.best-broadway-slider').slick({
      dots: false,
      infinite: false,
      speed: 800,
      slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            arrows:false,  
          }
        },  
     ]
    });
    jQuery('.live-music-slider').slick({
      dots: false,
      infinite: false,
      speed: 800,
      slidesToShow: 2,
      slidesToScroll: 1, 
      responsive: [
          {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            arrows:false,  
          }
        }, 
      ]
    });
    jQuery('.texas-highlights-slider').slick({
      dots: false,
      infinite: false,
      speed: 800,
      slidesToShow: 2,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            arrows:false,  
          }
        },  
     ]
    });
    jQuery('.music-festivals-slider').slick({
      dots: false,
      infinite: false,
      speed: 800,
      slidesToShow: 4,
      slidesToScroll: 1, 
        responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 767,
          settings: {
            infinite: false,
            slidesToShow: 1,
            arrows:false,  
          }
        }, 
     ]
    });
    
    jQuery('.concerts-tickets-slider').slick({
      dots: false,
      infinite: false,
      speed: 800,
      slidesToShow: 6,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3,
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            arrows:false,  
          }
        },  
     ]
    });
    jQuery('.master-testi .testimonials-list').slick({
      dots: false,
      infinite: true,
      speed: 800,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            arrows:false,
            dots: true,  
          }
        },  
     ]
    });
    jQuery('.tile-section-inner').slick({
      dots: false,
      infinite: false,
      speed: 800,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            arrows:false,
            dots: true, 
          }
        },  
     ]
    });
    var slideCount = jQuery('.slideCount');
    var projectGallery = jQuery('.master-gallery-slider');


     projectGallery.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    var i = (currentSlide ? currentSlide : 0) + 1;
    jQuery('.master-gallery').find('.slideCount').html('' + '<span class="slideCountItem">' + ( ((i)) ) + '</span> ' + 'of' + ' <span class="slideCountAll">' + (slick.slideCount ) + '</span>');
    });
    jQuery('.master-gallery-slider').slick({
      dots: false,
      infinite: true,
      speed: 800,
      slidesToShow: 1,
      slidesToScroll: 1,
      asNavFor: '.gallery-caption-slider',
      responsive: [
        {
          breakpoint: 767,
          settings: {
            arrows:false,
            dots: true,
          }
        }
      ]
    });
    jQuery('.gallery-caption-slider').slick({
      dots: false,
      infinite: false,
      speed: 800,
      fade: true,
      arrows:false,
      slidesToShow: 1,
      slidesToScroll: 1,
      asNavFor: '.master-gallery-slider',
    });


    jQuery(window).on('load resize orientationchange', function() {
            var $carousel = jQuery('.testimonials .testimonials-list');
            /* Initializes a slick carousel only on mobile screens */
            // slick on mobile
            if (jQuery(window).width() > 991) {
                if ($carousel.hasClass('slick-initialized')) {
                    $carousel.slick('unslick');
                }
            }
            else{
                if (!$carousel.hasClass('slick-initialized')) {
                    $carousel.slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots:true,
                        arrows:false,
                        mobileFirst: true,
                    });
                }
            }
            var $carousel2 = jQuery('.other-tickets-inner');
            /* Initializes a slick carousel only on mobile screens */
            // slick on mobile
            if (jQuery(window).width() > 767) {
                if ($carousel2.hasClass('slick-initialized')) {
                    $carousel2.slick('unslick');
                }
            }
            else{
                if (!$carousel2.hasClass('slick-initialized')) {
                    $carousel2.slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots:false,
                        arrows:false,
                        mobileFirst: true,
                    });
                }
            }
            var $carousel2 = jQuery('.master-grid-inner');
            /* Initializes a slick carousel only on mobile screens */
            // slick on mobile
            if (jQuery(window).width() > 767) {
                if ($carousel2.hasClass('slick-initialized')) {
                    $carousel2.slick('unslick');
                }
            }
            else{
                if (!$carousel2.hasClass('slick-initialized')) {
                    $carousel2.slick({
                        slidesToShow: 1,
                        infinite:false,
                        slidesToScroll: 1,
                        dots:true,
                        arrows:false,
                        mobileFirst: true,
                    });
                }
            }
            var $carousel2 = jQuery('.icon-text-inner');
            /* Initializes a slick carousel only on mobile screens */
            // slick on mobile
            if (jQuery(window).width() > 767) {
                if ($carousel2.hasClass('slick-initialized')) {
                    $carousel2.slick('unslick');
                }
            }
            else{
                if (!$carousel2.hasClass('slick-initialized')) {
                    $carousel2.slick({
                        slidesToShow: 1,
                        infinite:false,
                        slidesToScroll: 1,
                        dots:true,
                        arrows:false,
                        mobileFirst: true,
                    });
                }
            }
            
    });
    
    jQuery('.footer-links h6').click(function(){
       if(jQuery(window).width() < 768){
           jQuery(this).next('.footer-nav').slideDown().parent('.footer-links').siblings().find('.footer-nav').slideUp();;
           jQuery(this).addClass('active').parent('.footer-links').siblings().find('h6').removeClass('active');
       }  
    });
    jQuery('.event-location-toggle').click(function(){
       jQuery('.event-location-form').slideToggle(); 
    });
    jQuery('.menu-toggle').click(function(){
       jQuery('.main-navigation').slideToggle(); 
       jQuery('body').toggleClass('menu-open');    
    });
    jQuery('select').select2({});

    jQuery('.accordion-content').hide();
    jQuery('.accordion-item:first .accordion-content').show();
    jQuery('.accordion-item .title').click(function() {
      jQuery(this).toggleClass('active').parent('.accordion-item').siblings().children('.title').removeClass('active');
      jQuery(this).next('.accordion-content').slideToggle().parent('.accordion-item').siblings().children('.accordion-content').slideUp();
    });
    jQuery('.close-icon').click(function(){
       jQuery('.money-back-gurantee').hide();
       jQuery('body').css('margin-top', 0);
       jQuery('.site-header').css('top', 0);
    });
   topbar();
    
    
    jQuery('.moreless-button').click(function() {
      jQuery('.moretext').slideToggle();
      jQuery(this).hide();
    });
    
    jQuery('.readless-button').click(function(){
        jQuery('.moretext').slideUp();
        jQuery('.moreless-button').show();
    });
    
    
});
jQuery(window).on('load', function(){
    topbar();
});
jQuery(window).on('resize', function(){
    topbar();
});

function topbar(){
  var moneyH = jQuery('.money-back-gurantee').outerHeight();
  if(jQuery('.money-back-gurantee').is(':visible')){
     jQuery('body').css('margin-top', moneyH);
     jQuery('.site-header').css('top', moneyH);
  } else {
    jQuery('body').css('margin-top', 0);
    jQuery('.site-header').css('top', 0);
  }
}